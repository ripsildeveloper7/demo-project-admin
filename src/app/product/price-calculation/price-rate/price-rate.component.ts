import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../product.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { PriceRate } from '../../../shared/model/priceRate.model';
import { PriceRateService } from '../price-rate-popup/price-rate.service';

@Component({
  selector: 'app-price-rate',
  templateUrl: './price-rate.component.html',
  styleUrls: ['./price-rate.component.css']
})
export class PriceRateComponent implements OnInit {
  holder: any;
  hold: {
    priceRate: number,
  };
  selectedCode;
  selectedId;
  constructor(private router: Router, private productService: ProductService,
              public dialog: MatDialog, private priceRateService: PriceRateService) { 
    this.getPriceRate();
  }

  ngOnInit() {
  }
  getPriceRate() {
    this.productService.getPriceRate().subscribe(data => {
      this.holder = data;
    }, error => {
      console.log(error);
    });
  }
  addRate(rate) {
    this.hold = {
      priceRate: rate
    };
    this.productService.addPriceRate(this.hold).subscribe(data => {
      this.holder = data;
    }, error => {
      console.log(error);
    });
  }
  onNewCurrency() {
    this.priceRateService.open().subscribe(data => {
      if (data) {
        this.holder = data;
      }
    });
  }
  getCountry(country) {
    this.holder.forEach(a => {
      if (a.country === country.target.value) {
        this.selectedCode = a.countryCode;
        this.selectedId = a._id;
      }
    });
    console.log(country.target.value);
  }
  onSubmit(price) {
    const holder = new PriceRate();
    holder.amount = price;
    this.productService.UpdatePriceRate(holder, this.selectedId).subscribe(data => {
      this.holder = data;
    }, error => {
      console.log(error);
    });
  }
}
