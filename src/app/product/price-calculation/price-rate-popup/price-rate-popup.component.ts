import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { PriceRate } from '../../../shared/model/priceRate.model';
import { ProductService } from '../../product.service';

@Component({
  selector: 'app-price-rate-popup',
  templateUrl: './price-rate-popup.component.html',
  styleUrls: ['./price-rate-popup.component.css']
})
export class PriceRatePopupComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<PriceRatePopupComponent>,
              private productService: ProductService) {
    console.log(data);
  }

  ngOnInit() {
  }
  close() {
    this.dialogRef.close(false);
  }
  onCreate(country, code) {
    const holder = new PriceRate();
    holder.country = country;
    holder.countryCode = code;
    this.productService.addPriceRate(holder).subscribe(data => {
      this.dialogRef.close(data);
    }, error => {
      console.log(error);
    });
  }
}
